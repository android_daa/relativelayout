package cl.duoc.relativelayout;

/**
 * Created by LC1300XXXX on 06/09/2017.
 */

public class Provedor {
    private String nombre;
    private String empresa;
    private String telefono;

    public Provedor(String nombre, String empresa, String telefono) {
        this.nombre = nombre;
        this.empresa = empresa;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object p){
        if(p instanceof Provedor){
            Provedor aux = (Provedor)p;
            return aux.getEmpresa().equals(this.getEmpresa())
                    && aux.getNombre().equals(this.getNombre())
                    && aux.getTelefono().equals(this.getTelefono());
        }
        return false;
    }

   /* @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Provedor)) return false;

        Provedor provedor = (Provedor) o;

        if (!getNombre().equals(provedor.getNombre())) return false;
        if (!getEmpresa().equals(provedor.getEmpresa())) return false;
        return getTelefono().equals(provedor.getTelefono());

    }

    @Override
    public int hashCode() {
        int result = getNombre().hashCode();
        result = 31 * result + getEmpresa().hashCode();
        result = 31 * result + getTelefono().hashCode();
        return result;
    }*/
}
