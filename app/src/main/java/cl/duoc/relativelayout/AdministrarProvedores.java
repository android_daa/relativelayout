package cl.duoc.relativelayout;

import java.util.ArrayList;

/**
 * Created by LC1300XXXX on 09/09/2017.
 */

public class AdministrarProvedores {

    private static AdministrarProvedores instance;
    static {
        instance = new AdministrarProvedores();
        for (int x = 0; x < 500; x++) {
            instance.guardarProvedor(new Provedor("a"+x, "e"+x, "t"+x));
        }
    }
    private ArrayList<Provedor> values;

    protected AdministrarProvedores() {
        values = new ArrayList<>();
    }

    public static AdministrarProvedores getInstance() {
        if (instance == null) {
            instance = new AdministrarProvedores();
        }
        return instance;
    }

    public boolean guardarProvedor(Provedor provedor) {
        if(!buscarProvedor(provedor)) {
            values.add(provedor);
            return true;
        }else {
            return false;
        }
    }

    public boolean buscarProvedor(Provedor provedor) {
        for (Provedor aux : values) {
            if (provedor.equals(aux)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Provedor> listarProvedores(){
        return values;
    }
}
