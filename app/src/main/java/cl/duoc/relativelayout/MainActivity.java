package cl.duoc.relativelayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre, etTelefono, etEmpresa;

    private Button btnGuardar;
    private Button btnListar;

    private ArrayList<Provedor> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        datos = new ArrayList<>();
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AdministrarProvedores.getInstance().guardarProvedor(obtenerProvedor())){
                    Toast.makeText(MainActivity.this, "Provedor Guardado correctamente", Toast.LENGTH_SHORT).show();
                    limpiarTextos();
                }else{
                    Toast.makeText(MainActivity.this, "Provedor ya existe", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnListar = (Button)findViewById(R.id.btnListar);
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirListadoDeProvedores();
            }
        });

        etNombre = (EditText) findViewById(R.id.etNombre);
        etEmpresa = (EditText) findViewById(R.id.etEmpresa);
        etTelefono = (EditText) findViewById(R.id.etTelefono);
    }

    private void abrirListadoDeProvedores() {
        Intent i = new Intent(this, ListarProvedoresActivity.class);
        startActivity(i);
    }


    private void limpiarTextos() {
        etEmpresa.setText("");
        etNombre.setText("");
        etTelefono.setText("");
        etNombre.requestFocus();
    }

    private Provedor obtenerProvedor() {
        return new Provedor(getTextET(etNombre), getTextET(etEmpresa), getTextET(etTelefono));
    }

    private String getTextET(EditText et) {
        return et.getText().toString();
    }


}
