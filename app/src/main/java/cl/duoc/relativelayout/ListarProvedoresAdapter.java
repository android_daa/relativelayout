package cl.duoc.relativelayout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by LC1300XXXX on 09/09/2017.
 */

public class ListarProvedoresAdapter extends BaseAdapter {

    private ArrayList<Provedor> values;
    private Context context;

    public ListarProvedoresAdapter(ArrayList<Provedor> values, Context context) {
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            convertView = inflater.inflate(R.layout.item_provedores, parent, false);

            holder = new ViewHolder();
            holder.tvEmpresa = (TextView) convertView.findViewById(R.id.tvEmpresa);
            holder.tvNombre = (TextView) convertView.findViewById(R.id.tvNombreProvedor);
            holder.tvTelefono = (TextView) convertView.findViewById(R.id.tvNroTelefono);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Provedor aux = values.get(position);
        holder.tvNombre.setText(aux.getNombre());
        holder.tvEmpresa.setText(aux.getEmpresa());
        holder.tvTelefono.setText(aux.getTelefono());

        return convertView;
    }

    private static class ViewHolder {
        TextView tvNombre;
        TextView tvEmpresa;
        TextView tvTelefono;
    }
}