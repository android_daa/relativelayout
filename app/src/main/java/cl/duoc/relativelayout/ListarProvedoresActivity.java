package cl.duoc.relativelayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ListarProvedoresActivity extends AppCompatActivity {

    private ListView lvProvedores;
    private ListarProvedoresAdapter listarProvedoresAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_provedores_activity);
        lvProvedores = (ListView)findViewById(R.id.lvProvedores);
        listarProvedoresAdapter = new ListarProvedoresAdapter(
                AdministrarProvedores.getInstance().listarProvedores(),
                this
        );
        lvProvedores.setAdapter(listarProvedoresAdapter);
    }
}
